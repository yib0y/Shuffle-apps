import paramiko
from paramiko.ssh_exception import NoValidConnectionsError
from paramiko.ssh_exception import AuthenticationException
from walkoff_app_sdk.app_base import AppBase
class SshTools(AppBase):
    __version__ = "1.0.0"
    app_name = "ssh tools"

    def __init__(self, redis, logger, console_logger=None):
        """
        Each app should have this __init__ to set up Redis and logging.
        :param redis:
        :param logger:
        :param console_logger:
        """
        super().__init__(redis, logger, console_logger)

    def remote_ssh(self, hostname, port, username, password, execmd):
        s = None
        paramiko.util.log_to_file("paramiko.log")
        try:
            s = paramiko.SSHClient()
            s.set_missing_host_key_policy(paramiko.AutoAddPolicy())

            s.connect(hostname=hostname, port=int(port), username=username, password=password)
            stdin, stdout, stderr = s.exec_command(execmd)
            stdin.write("Y")  # Generally speaking, the first connection, need a simple interaction.
            info = {'status': 'true', 'info': str(stdout.read())}
        except NoValidConnectionsError as e:  ###用户不存在时的报错
            info = {'status': 'false', 'info': e, 'message': '用户不存在'}
        except AuthenticationException as t:  ##密码错误的报错
            info = {'status': 'false', 'info': t, 'message': '密码错误'}
        s.close()
        return info

# Run the actual thing after we've checked params
def run(request):
    action = request.get_json()
    print(action)
    print(type(action))
    authorization_key = action.get("authorization")
    current_execution_id = action.get("execution_id")

    if action and "name" in action and "app_name" in action:
        SshTools.run(action)
        return f'Attempting to execute function {action["name"]} in app {action["app_name"]}'
    else:
        return f'Invalid action'


if __name__ == "__main__":
    SshTools.run()
