import requests
from http.client import HTTPConnection

HTTPConnection.debuglevel = 1
import pymysql
from walkoff_app_sdk.app_base import AppBase

import json
from datetime import date, datetime

class DateEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, datetime):
            return obj.strftime('%Y-%m-%d %H:%M:%S')
        elif isinstance(obj, date):
            return obj.strftime("%Y-%m-%d")
        else:
            return json.JSONEncoder.default(self, obj)
class MysqlTools(AppBase):
    __version__ = "1.0.0"
    app_name = "mysql tools"

    def __init__(self, redis, logger, console_logger=None):
        """
        Each app should have this __init__ to set up Redis and logging.
        :param redis:
        :param logger:
        :param console_logger:
        """
        super().__init__(redis, logger, console_logger)

    def selectOne(self, sql, dbHost, dbUser, dbPass, dbName, dbPort):
        db = None
        try:
            db = pymysql.connect(host=dbHost, user=dbUser, password=dbPass, database=dbName, port=int(dbPort), charset='utf8')
            cursor = db.cursor(pymysql.cursors.DictCursor)
            cursor.execute(sql)
            # 使用 fetchone() 方法获取一条数据
            data = cursor.fetchone()
            return {'status':'true','info': json.dumps(data, cls=DateEncoder)}
        except pymysql.Error as e:
            print("数据库连接失败：" + str(e))
            return {'status':'false','info':str(e)}
        finally:
            if db:
                db.close()
                print('关闭数据库连接....')

    def selectAll(self, sql, dbHost, dbUser, dbPass, dbName, dbPort):
        db = None
        try:
            db = pymysql.connect(host=dbHost, user=dbUser, password=dbPass, database=dbName, port=int(dbPort), charset='utf8')
            cursor = db.cursor(pymysql.cursors.DictCursor)
            cursor.execute(sql)
            # 使用 fetchone() 方法获取所有数据
            data = cursor.fetchall()
            return {'status':'true','info': json.dumps(data, cls=DateEncoder)}
        except pymysql.Error as e:
            print("数据库连接失败：" + str(e))
            return {'status':'false','info':str(e)}
        finally:
            if db:
                db.close()
                print('关闭数据库连接....')

    def insert(self, sql, dbHost, dbUser, dbPass, dbName, dbPort):
        self.base(sql, dbHost, dbUser, dbPass, dbName, dbPort)

    def update(self, sql, dbHost, dbUser, dbPass, dbName, dbPort):
        self.base(sql, dbHost, dbUser, dbPass, dbName, dbPort)

    def delete(self, sql, dbHost, dbUser, dbPass, dbName, dbPort):
        self.base(sql, dbHost, dbUser, dbPass, dbName, dbPort)

    def base(self, sql, dbHost, dbUser, dbPass, dbName, dbPort):
        db = None
        try:
            db = pymysql.connect(host=dbHost, user=dbUser, password=dbPass, database=dbName, port=int(dbPort), charset='utf8')
            cursor = db.cursor(pymysql.cursors.DictCursor)
            cursor.execute(sql)
            # 使用 fetchone() 方法获取所有数据
            db.commit()
            return {'status':'true','info':'1'}
        except pymysql.Error as e:
            print("数据库连接失败：" + str(e))
            return {'status':'false','info':str(e)}
        finally:
            if db:
                db.close()
                print('关闭数据库连接....')

# Run the actual thing after we've checked params
def run(request):
    action = request.get_json()
    print(action)
    print(type(action))
    authorization_key = action.get("authorization")
    current_execution_id = action.get("execution_id")

    if action and "name" in action and "app_name" in action:
        MysqlTools.run(action)
        return f'Attempting to execute function {action["name"]} in app {action["app_name"]}'
    else:
        return f'Invalid action'


if __name__ == "__main__":
    MysqlTools.run()
