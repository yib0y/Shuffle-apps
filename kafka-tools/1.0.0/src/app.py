from walkoff_app_sdk.app_base import AppBase
from kafka import KafkaProducer
import json
class KafkaTools(AppBase):
    __version__ = "1.0.0"
    app_name = "kafka tools"

    def __init__(self, redis, logger, console_logger=None):
        """
        Each app should have this __init__ to set up Redis and logging.
        :param redis:
        :param logger:
        :param console_logger:
        """
        super().__init__(redis, logger, console_logger)

    def sendMsg(self, hosts, topic, msg):
        producer = KafkaProducer(
            value_serializer=lambda v: json.dumps(v).encode('utf-8'),
            bootstrap_servers=json.loads(hosts)
        )
        producer.send(topic, msg)
        producer.close()
        return {'status':'true'}
# Run the actual thing after we've checked params
def run(request):
    action = request.get_json()
    print(action)
    print(type(action))
    authorization_key = action.get("authorization")
    current_execution_id = action.get("execution_id")

    if action and "name" in action and "app_name" in action:
        KafkaTools.run(action)
        return f'Attempting to execute function {action["name"]} in app {action["app_name"]}'
    else:
        return f'Invalid action'


if __name__ == "__main__":
    KafkaTools.run()
