import ssl, socket
from urllib.parse import urlparse
import json
import datetime
import os
import base64
import time
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from walkoff_app_sdk.app_base import AppBase
def formatGMTime(timestamp):
    GMT_FORMAT = '%b %d %H:%M:%S %Y GMT'
    a = datetime.datetime.strptime(timestamp, GMT_FORMAT) + datetime.timedelta(hours=8)
    return str(a)
class DomainTools(AppBase):
    __version__ = "1.0.0"
    app_name = "domain tools"

    def __init__(self, redis, logger, console_logger=None):
        """
        Each app should have this __init__ to set up Redis and logging.
        :param redis:
        :param logger:
        :param console_logger:
        """
        super().__init__(redis, logger, console_logger)

    def get_cert_info(self, url):
        if "https://" in url:
            domain = urlparse(url).netloc
            hostname = domain
        else:
            hostname = url
        print(hostname)
        c = ssl.create_default_context()
        s = c.wrap_socket(socket.socket(), server_hostname=hostname)
        s.connect((hostname, 443))
        cert = s.getpeercert()
        cert['notBefore'] = formatGMTime(cert.get('notBefore'))
        cert['notAfter'] = formatGMTime(cert.get('notAfter'))
        subjectAltName = s.getpeercert().get('subjectAltName')
        list = []
        for i in subjectAltName:
            list.append(i[1])
        cert['subjectAltName'] = list
        j = json.dumps(cert)
        return j

    def get_ip_list(self, domain):  # 获取域名解析出的IP列表
        ip_list = []
        try:
            addrs = socket.getaddrinfo(domain, None)
            for item in addrs:
                if item[4][0] not in ip_list:
                    ip_list.append(item[4][0])
        except Exception as e:
            # print(str(e))
            pass
        return ip_list

    def get_domain_image(self, url):
        """
        #设置chrome开启的模式，headless就是无界面模式
        # 创建一个参数对象，用来控制chrome以无界面模式打开
        :param url:             获取获取网页的地址
        :param pic_name:        需要保存的文件名或路径＋文件名
        :return:
        """
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        # 修复一个bug
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        # 创建浏览器对象
        driver = webdriver.Chrome(executable_path='/usr/bin/chromedriver',
                                  chrome_options=chrome_options)
        # 打开网页
        driver.get(url)
        # driver.maximize_window()
        # 加延时 防止未加载完就截图
        time.sleep(10)
        # 用js获取页面的宽高，如果有其他需要用js的部分也可以用这个方法
        width = driver.execute_script("return document.documentElement.scrollWidth")
        height = driver.execute_script("return document.documentElement.scrollHeight")
        # 获取页面宽度及其宽度
        print(width, height)
        # 将浏览器的宽高设置成刚刚获取的宽高
        driver.set_window_size(width, height)
        time.sleep(1)
        # 截图并关掉浏览器
        driver.get_screenshot_as_file("result.png")
        driver.quit()
        ## 返回图片
        filepath = "/app/result.png"
        filename = "result.png"
        filedata = {
            "data": open(filepath, 'rb').read(),
            "filename": filename,
        }
        ret = self.set_files(filedata)
        self.logger.info(ret)
        self.logger.info("梁乙 bug test")
        if len(ret) == 1:
            return {
                    "success": True,
                    "file_id": ret[0],
                    "filename": filename,
                    "url": "https://10.0.241.46:3443/api/v1/files/"+str(ret[0])+"/content"
            }
        else:
            return {
                "success": False,
                "reason": "Bad return from file upload: %s" % ret
            }
        # with open(filepath, 'rb') as f:
        #     image = f.read()
        #     image_base64 = base64.b64encode(image)
        #     s = image_base64.decode()
        #     result = 'data:image/png;base64,%s' % s
        #     os.system(f'rm -rf ' + filepath)
        #     return result

def run(request):
    action = request.get_json()
    authorization_key = action.get("authorization")
    current_execution_id = action.get("execution_id")

    if action and "name" in action and "app_name" in action:
        DomainTools.run(action)
        return f'Attempting to execute function {action["name"]} in app {action["app_name"]}'
    else:
        return f'Invalid action'
if __name__ == "__main__":
    DomainTools.run()